/****************************************************

CONDUCTImtero - CoSensores (Sensores Comunitarios) 

https://gitlab.com/cosensores
https://www.facebook.com/cosensores/
https://www.instagram.com/cosensores/

Somos miembros de Universidades Nacionales trabajando junto a comunidades organizadas 
en el desarrollo de métodos para evaluar la presencia de contaminantes 
de manera sencilla en el territorio, acompañando acciones y procesos reivindicativos.

*****************************************************
Adaptación del codigo de DFRobot Gravity: Analog TDS Sensor/Meter 
https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_TDS_Sensor_/_Meter_For_Arduino_SKU:_SEN0244

 1. El codigo fue evaluado en Arduino Uno - IDE 1.0.5
 2. Para calibrar:
     CALIBRAR -> ingresa al modo calibracio'n 
     CAL:"valor de conductividad del patro'n" -> se recomienda sc 692 mg/L NaCl = 1413 uS/cm a 25^c 
     SALIR -> guarda los parametros y sale del modo de calibracio'n
     
****************************************************
Librerias control sensor temperatura: ds18b20
https://github.com/PaulStoffregen/OneWire
https://github.com/milesburton/Arduino-Temperature-Control-Library

****************************************************/

//temperatura
#include <OneWire.h>                
#include <DallasTemperature.h>
OneWire ourWire(2);                //conectar sensor temperatura a pin digital 2
DallasTemperature sensors(&ourWire); //declara una variable u objeto para nuestro sensor

//conductividad
#include <EEPROM.h>
#include "GravityTDS.h"
#define TdsSensorPin A4             //conectar sensor conductividad en pin A4
GravityTDS gravityTds;

float temperature = 25,tdsValue = 0, ecValue25 = 0;

void setup()
{ 
Serial.begin(9600);

//tempratura
sensors.begin();   //Se inicia el sensor

//conductividad
    gravityTds.setPin(TdsSensorPin);
    gravityTds.setAref(5.0);       //voltage de referencia en ADC, default 5.0V en Arduino UNO
    gravityTds.setAdcRange(1024);  //1024 para 10bit ADC;4096 para 12bit ADC
    gravityTds.begin();            //initializacio'n  
}


void loop()
{

//temperatura  
sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
float temp= sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC

//conductividad
    temperature = temp;       //comentar para no levantar temperatura
    gravityTds.setTemperature(temperature);  //define temperatura para compensacio'n del valor de consuctividad
    gravityTds.update();                     //lee y calcula
    //tdsValue = gravityTds.getTdsValue();   //valor de conducividad en ppm (tds)
    //Serial.print(tdsValue,0);
    //Serial.println(" ppm");
    ecValue25 = gravityTds.getEcValue();     //valor de conducividad en uS/cm
    
//imprime
    Serial.print(ecValue25,0);
    Serial.println(" uS/cm");
    Serial.print(temp);
    Serial.println(" Celsius");
    delay(2000);
}
